// ==UserScript==
// @name         防top站盗号
// @version      0.1
// @description  使用前请先清空浏览器cookie或者退出登录再开启脚本！.
// @author       sparkle
// @match        *://*/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    function isTopCodemao(url) {
        var parser = document.createElement('a');
        parser.href = url;
        return parser.hostname === 'top.codemao.cn';
    }

    function isTopDomain(url){
        return url.includes("api.codemao.cn/tiger/v3/web/accounts/tokens/refresh");
    }

    function blockRequest(target, url) {
        console.log("Blocked " + target + " load from token reset api:", url);
        if (target === "iframe") {
            iframe.src = 'about:blank';
        } else if (target === "embed") {
            embed.remove();
        }
    }

    var open = window.XMLHttpRequest.prototype.open;
    window.XMLHttpRequest.prototype.open = function() {
        if (arguments[1] && isTopDomain(arguments[1])) {
            console.log("Blocked request to top.codemao.cn:", arguments[1]);
            return;
        }
        open.apply(this, arguments);
    };

    var fetch = window.fetch;
    window.fetch = function() {
        var url = arguments[0];
        if (url && typeof url === 'string' && isTopDomain(url)) {
            console.log("Blocked fetch request to top.codemao.cn:", url);
            return Promise.reject("Blocked by user script");
        }
        return fetch.apply(this, arguments);
    };

    var iframes = document.getElementsByTagName('iframe');
    for (var i = 0; i < iframes.length; i++) {
        var iframe = iframes[i];
        if (isTopCodemao(iframe.src)) {
            blockRequest("iframe", iframe.src);
        }
    }

    var embeds = document.getElementsByTagName('embed');
    for (var j = 0; j < embeds.length; j++) {
        var embed = embeds[j];
        if (isTopCodemao(embed.src)) {
            blockRequest("embed", embed.src);
        }
    }
})();